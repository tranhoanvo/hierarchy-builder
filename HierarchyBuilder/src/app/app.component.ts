import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DatabaseService } from './database.service'
import { fader } from './route-animation';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fader]
})

export class AppComponent {

  constructor(private router: Router, private fetchdatabase: DatabaseService) { }

  title = 'HierarchyBuilder';
  page = 1;
  pageSize = 5;

  database;
  name = sessionStorage.name;
  doneLoading = true;
  showAlert = false;
  alertDisplay = false;

  username;
  password;
  account = [
    {
      username: "admin",
      password: "admin",
      name: "Administrator"
    },
    {
      username: "hoanvt",
      password: "longrio2",
      name: "Tran Hoan Vo"
    }
  ];

  async ngOnInit() {
    this.database = await this.fetchdatabase.fetchDB();

    if (!sessionStorage.name) {
      sessionStorage.name = 'Guest';
      this.name = sessionStorage.name;
    }

    if (sessionStorage.theme != 'dark') { sessionStorage.theme = 'light'; document.documentElement.classList.remove('dark'); }
    else document.documentElement.classList.add('dark');

    this.setAlertClose();
  }

  async setAlertClose() {
    const delay = ms => new Promise(res => setTimeout(res, ms));
    await delay(1000);
    this.alertDisplay = true;
    await delay(300);
    this.showAlert = true;
    await delay(6000);
    this.alertAction();
  }

  async alertAction() {
    const delay = ms => new Promise(res => setTimeout(res, ms));
    this.showAlert = false;
    await delay(300);
    this.alertDisplay = false;
  }

  async login() {
    const delay = ms => new Promise(res => setTimeout(res, ms));
    this.doneLoading = false;
    await delay(1000);
    this.doneLoading = true;
    if (this.username && this.password) {
      this.account.forEach(acc => {
        if (this.username == acc.username && this.password == acc.password) {
          location.reload();
          sessionStorage.setItem('name', acc.name);
          this.name = sessionStorage.name;
        }
      })
    }
  }

  signOut() {
    location.reload();
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('password');
    sessionStorage.removeItem('name');
  }

  getscrollTop() {
    if (this.isActiveLink('/'))
      return document.getElementById('Tesst').scrollTop >= 75;
    else return document.getElementById('Tesst').scrollTop >= 0;
  }

  isActiveLink(link) {
    return this.router.url == link;
  }

  changeTheme() {
    if (sessionStorage.theme == 'light') { sessionStorage.theme = 'dark'; document.documentElement.classList.add('dark'); }
    else { sessionStorage.theme = 'light'; document.documentElement.classList.remove('dark'); };
  }

  returnTheme() {
    return sessionStorage.theme;
  }

  deleteHierarchyAsk(database, hierarchyId) {
    Swal.fire({
      background: 'var(--a-color)',
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire({
          background: 'var(--a-color)',
          icon: 'success',
          title: 'Deleted Hierarchy',
          showConfirmButton: false,
          timer: 750
        });
        this.deleteHierarchy(database, hierarchyId);
      }
    })
  }

  deleteHierarchy(database, hierarchyId) {
    for (let index = 0; index < database.length; index++) {
      if (database[index].id == hierarchyId) {
        database.splice(index, 1);
      }
    }
    for (let index = 0; index < database.length; index++) {
      database[index].id = index + 1;
    }
    this.fetchdatabase.saveDB(database);
  }

}
