import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DatabaseService } from '../database.service';
import Swal from 'sweetalert2';

interface databaseObj {
  name: string;
  dob: string;
  children: [];
}

function deleteMember(databaseNew, memberName) {
  for (let index = 0; index < databaseNew.length; index++) {
    if (databaseNew[index].name == memberName) {
      databaseNew.splice(index, 1);
    } else {
      deleteMember(databaseNew[index].children, memberName);
    }
  }

}

function searchTree(databaseNew, parentName, item) {
  if (parentName == null) {
    databaseNew.push(item);
  } else for (let index = 0; index < databaseNew.length; index++) {
    if (parentName == databaseNew[index].name) {
      databaseNew[index].children.push(item);
    } else {
      searchTree(databaseNew[index].children, parentName, item);
    }
  }
}

function listTree(databaseNew, listName) {
  for (let index = 0; index < databaseNew.length; index++) {
    listName.push(databaseNew[index].name);
    if (databaseNew[index].children.length > 0) {
      listTree(databaseNew[index].children, listName);
    }
  }
  return listName;
}

function setDisabled(value) {
  var x = document.getElementsByName('FormSubmitButton');
  for (let index = 0; index < x.length; index++) {
    (x[index] as HTMLButtonElement).disabled = value;
  }
}

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})

export class CreateComponent implements OnInit {

  constructor(private route: ActivatedRoute, private http: HttpClient, private fetchdatabase: DatabaseService) { }

  hierarchyId;
  listName = [];
  database: any = [];
  databaseNew = [];


  inputName;
  inputDoB;

  ngOnInit(): void {
    this.route.params.subscribe(
      data => {
        this.hierarchyId = data.id;
        if (this.hierarchyId != 0) {
          this.fetchdatabase.fetchDB().then(data => {
          this.database = data;
          this.databaseNew = this.database[(this.hierarchyId - 1)].hierarchyData;
          this.listName = listTree(this.databaseNew, this.listName);
        });
        }
      }
    );
  }

  saveHierarchy() {
    Swal.fire({
      background: 'var(--a-color)',
      icon: 'success',
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1000
    })
    this.database[(this.hierarchyId - 1)].hierarchyData = this.databaseNew;
    this.fetchdatabase.saveDB(this.database);
  }

  createNew(parentName) {
    let item: databaseObj = { name: this.inputName, dob: this.inputDoB, children: [] };
    searchTree(this.databaseNew, parentName, item);

    this.listName = listTree(this.databaseNew, this.listName);
    this.inputName = '';
    this.inputDoB = '';
  }

  deleteMemberAsk(databaseNew, memberName) {
    Swal.fire({
      background: 'var(--a-color)',
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire({
          background: 'var(--a-color)',
          icon: 'success',
          title: 'Deleted Member(s)',
          showConfirmButton: false,
          timer: 750
        });
        deleteMember(databaseNew, memberName);
      }
    })
  }

  onTyping() {
    if (this.listName.find(element => element === this.inputName)) {
      setDisabled(true);
    } else setDisabled(false);
  }

}
