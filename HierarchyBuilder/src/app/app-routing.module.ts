import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { AppComponent } from './app.component';



const routes: Routes = [
  { path: '', component: AppComponent, pathMatch: 'full'},
  { path: 'create/:id', component: CreateComponent, pathMatch: 'full'},
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes),],
  exports: [RouterModule]
})
export class AppRoutingModule { }
