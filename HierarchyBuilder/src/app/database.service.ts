import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(private http: HttpClient) { }

  database: any = [];

  public async fetchDB() {
    if(!this.database[0]) {;this.database = await this.http.get('assets/database.json').toPromise();}
    return this.database;
  }

  public saveDB(databaseNew) {
    this.database = databaseNew;
  }



}
